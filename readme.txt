Curso de Udemy "FLEXBOX DESDE 0" por Erick Mines
Duración: 2 horas

NOTAS:
+ Cajas flexibles, su tamaño o anchura cambian de acuerdo a las necesidades del diseño usando este nuevo modelo de maquetado.
+ Sistema de propiedades que nos ayudan a crear interfaces web modificnando unas cuantas propiedades para hacer diseño responsive

HERRAMIENTAS DE TESTEO DE LAS PROPIEDADES:
* http://flexbox.help/
* https://the-echoplex.net/flexyboxes/
* https://css-tricks.com/snippets/css/a-guide-to-flexbox/

¿Qué podemos hacer con flexbox?
- Diseños flexibles y complejos con menos código
- Alinear los elementos de manera vertical y horizontal
- Re-ordenar contenido sin tocar etiquetas HTML

REGLAS: 
1. Flexbox necesita un padre y por lo menos un hijo. La anchura de cada uno de los hijos va a depender del tamaño del contenido y la altura va a ser del tamaño del contenedor
2. El Flex Container tiene dos ejes: el eje principa que es horizontal (x) y el eje secundario que es vertical (y). La definición de los ejes va a determinar como los hijos se van a mover dentro del flex container
* Flex Container (padre flexible), flex item (hijos flexibles)
3. Podemos modificar el eje principal: flex-direction
	: flex-direction: row /*alineación horizontal, siendo este el eje principal*/ 
	:: flex-direction: column /*alineación vertical, siendo este el eje principal*/ 
4. Podemos permitir el salto de columnas con la propiedad Flex-wrap:
	> flex-wrap:nowrap; /*la trae por defecto*/
	>> flex-wrap:wrap; /*para que se adapten al tamaño del contenedor*/
	>>> flex-wrap:wrap-reverse;
5. Alineamos elementos en el eje principal con justify-content.
	a) justify-content: space-between; /*coloca el primer elemento al comienzo y el ultimo elemento al final, distribuyendo los demas elementos de forma proporcional, siendo muy útil en los menus de forma horizontal*/
	b) justify-content: flex-end; /*alineados al final*/
	c) justify-content: flex-start; /*valor por defecto, alineados al comienzo*/
	d) justify-content: center; /*centramos los hijos de manera horizontal*/
	e) justify-content: space-around; /*usado normalmente para galeria de imágenes. Crea un espaciado al comienzo y al final de cada caja de forma proporcional*/
6. La alineación de los elementos en el eje secundario la realizamos con la propiedad align-items, permitiendo definir la orientación de los elementos en el eje secudario
	I) flex-start /*se van hacia arriba*/
	II) flex-end /*se van hacia abajo*/
	III) center /*se centra de acuerdo a la altura*/
	IV) stretch /*valor por defecto; los hijos ocupan todo el espacio que el padre les proporciona, en este caso la altura*/
	V) baseline 
7. Podemos alinear los elementos hijos de forma individual en el eje secundario con align-self 
8. Los hijos flexibles ignoran propiedades como float, clear, vertical-align
9. Podemos modificar el tamaño de los hijos con flex-grow, flex-shrink, flex-basis.
	Por defecto el tamaño de los hijos se define por el tamaño de su contenido, a mayor contenido, mayor es la dimensión.
	+ flex-grow: define el tamaño que crecerá un hijo, en relación a sus demás hermanos, cuando hay espacio disponible en el contenedor. Su VALOR por defecto es '0'.
	+ flex-shrink: define el tamaño de reducción de un hijo en relación a sus demás hermanos, cuando falte espacio en el contenedor. Por defecto es '1'.
	+ flex-basis: define el tamaño inicial del hijo. Su valor por defecto es "auto". Similar a la propiedad width
10. Podemos resumir todo con la propiedad flex: flex-grow, flex-shrink, flex-basis (Ejem: flex 0 1 100px;)
11. Podemos reordenar los hijos flexibles con la propiedad ORDER. El orden va de menor a mayor.También se pueden asignar valores negatiovs como -1.	El valor por defecto es 0.